﻿namespace ConstruCiudad
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtReportre = new System.Windows.Forms.Button();
            this.BtProyecto = new System.Windows.Forms.Button();
            this.BtEmpleado = new System.Windows.Forms.Button();
            this.BtCliente = new System.Windows.Forms.Button();
            this.BtMaterial = new System.Windows.Forms.Button();
            this.BtCiudad = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtReportre
            // 
            this.BtReportre.BackgroundImage = global::ConstruCiudad.Properties.Resources.Win7_Chrome_1920x1200;
            this.BtReportre.Location = new System.Drawing.Point(378, 97);
            this.BtReportre.Name = "BtReportre";
            this.BtReportre.Size = new System.Drawing.Size(117, 74);
            this.BtReportre.TabIndex = 0;
            this.BtReportre.Text = "Reporte";
            this.BtReportre.UseVisualStyleBackColor = true;
            this.BtReportre.Click += new System.EventHandler(this.BtReportre_Click);
            // 
            // BtProyecto
            // 
            this.BtProyecto.BackgroundImage = global::ConstruCiudad.Properties.Resources.Win7_Chrome_1920x1200;
            this.BtProyecto.Location = new System.Drawing.Point(148, 97);
            this.BtProyecto.Name = "BtProyecto";
            this.BtProyecto.Size = new System.Drawing.Size(117, 74);
            this.BtProyecto.TabIndex = 1;
            this.BtProyecto.Text = "Proyecto";
            this.BtProyecto.UseVisualStyleBackColor = true;
            this.BtProyecto.Click += new System.EventHandler(this.BtProyecto_Click);
            // 
            // BtEmpleado
            // 
            this.BtEmpleado.BackgroundImage = global::ConstruCiudad.Properties.Resources.Win7_Chrome_1920x1200;
            this.BtEmpleado.Location = new System.Drawing.Point(24, 251);
            this.BtEmpleado.Name = "BtEmpleado";
            this.BtEmpleado.Size = new System.Drawing.Size(117, 74);
            this.BtEmpleado.TabIndex = 2;
            this.BtEmpleado.Text = "Empleado";
            this.BtEmpleado.UseVisualStyleBackColor = true;
            this.BtEmpleado.Click += new System.EventHandler(this.BtEmpleado_Click);
            // 
            // BtCliente
            // 
            this.BtCliente.BackgroundImage = global::ConstruCiudad.Properties.Resources.Win7_Chrome_1920x1200;
            this.BtCliente.Location = new System.Drawing.Point(148, 384);
            this.BtCliente.Name = "BtCliente";
            this.BtCliente.Size = new System.Drawing.Size(117, 74);
            this.BtCliente.TabIndex = 3;
            this.BtCliente.Text = "Cliente";
            this.BtCliente.UseVisualStyleBackColor = true;
            this.BtCliente.Click += new System.EventHandler(this.BtCliente_Click);
            // 
            // BtMaterial
            // 
            this.BtMaterial.BackgroundImage = global::ConstruCiudad.Properties.Resources.Win7_Chrome_1920x1200;
            this.BtMaterial.Location = new System.Drawing.Point(505, 251);
            this.BtMaterial.Name = "BtMaterial";
            this.BtMaterial.Size = new System.Drawing.Size(117, 74);
            this.BtMaterial.TabIndex = 4;
            this.BtMaterial.Text = "Materiales";
            this.BtMaterial.UseVisualStyleBackColor = true;
            this.BtMaterial.Click += new System.EventHandler(this.BtMaterial_Click);
            // 
            // BtCiudad
            // 
            this.BtCiudad.BackgroundImage = global::ConstruCiudad.Properties.Resources.Win7_Chrome_1920x1200;
            this.BtCiudad.Location = new System.Drawing.Point(378, 384);
            this.BtCiudad.Name = "BtCiudad";
            this.BtCiudad.Size = new System.Drawing.Size(117, 74);
            this.BtCiudad.TabIndex = 5;
            this.BtCiudad.Text = "Ciudad";
            this.BtCiudad.UseVisualStyleBackColor = true;
            this.BtCiudad.Click += new System.EventHandler(this.BtCiudad_Click);
            // 
            // button7
            // 
            this.button7.BackgroundImage = global::ConstruCiudad.Properties.Resources.Win7_Chrome_1920x1200;
            this.button7.Location = new System.Drawing.Point(576, 508);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(89, 29);
            this.button7.TabIndex = 6;
            this.button7.Text = "Salir";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::ConstruCiudad.Properties.Resources.Win7_Chrome_1920x1200;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(677, 549);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.BtCiudad);
            this.Controls.Add(this.BtMaterial);
            this.Controls.Add(this.BtCliente);
            this.Controls.Add(this.BtEmpleado);
            this.Controls.Add(this.BtProyecto);
            this.Controls.Add(this.BtReportre);
            this.Name = "Menu";
            this.Text = "Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtReportre;
        private System.Windows.Forms.Button BtProyecto;
        private System.Windows.Forms.Button BtEmpleado;
        private System.Windows.Forms.Button BtCliente;
        private System.Windows.Forms.Button BtMaterial;
        private System.Windows.Forms.Button BtCiudad;
        private System.Windows.Forms.Button button7;
    }
}