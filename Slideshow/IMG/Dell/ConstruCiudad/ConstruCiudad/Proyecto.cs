﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ConstruCiudad
{
    public partial class Proyecto : Form
    {
        public Proyecto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        public void cargarGrid()
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

            DataTable dt = new DataTable();
            SqlCommand consultar = new SqlCommand("SELECT * FROM proyecto", con);
            SqlDataAdapter da = new SqlDataAdapter(consultar);


            da.Fill(dt);

            consultar.Connection = con;
            dataGridView1.DataSource = dt;
            consultar.ExecuteNonQuery();
        }

        private void Proyecto_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");
            try
            {
                SqlCommand comando = new SqlCommand("DELETE proyecto WHERE id_proyecto=@id_proyecto", con);
                comando.Parameters.Add("proyecto", SqlDbType.NVarChar, 50).Value = TxbId_proyecto.Text;
                comando.ExecuteNonQuery();
                cargarGrid();
            }
            catch (Exception)
            {
                MessageBox.Show("Elimina por medio de (proyecto)");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

                DataTable dt = new DataTable();
                SqlCommand consultar = new SqlCommand("SELECT * FROM proyecto", con);
                SqlDataAdapter da = new SqlDataAdapter(consultar);
                da.Fill(dt);
                consultar.Connection = con;
                dataGridView1.DataSource = dt;
                consultar.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
