﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ConstruCiudad
{
    public partial class materiales : Form
    {
        public materiales()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

            if (TxbId_Materiales.Text == "")
            {
                MessageBox.Show("el campo id_meteriales esta  vacion");
                TxbId_Materiales.Focus();

            }
            else if (TxbDescripcion.Text == "")
            {
                MessageBox.Show("el campo descripcion esta vacio");
                TxbDescripcion.Focus();
            }
            else if (TxbCostos.Text == "")
            {
                MessageBox.Show("el campo costos esta vacio");
                TxbCostos.Focus();
            }
            else if (TxbUnidad.Text == "")
            {
                MessageBox.Show("el campo unidad esta vacio");
                TxbUnidad.Focus();
            }
            else if (TxbStockminimo.Text == "")
            {
                MessageBox.Show("el campo stockminimo documento esta vacio");
                TxbStockminimo.Focus();
            }
            else if (TxbExistencias.Text == "")
            {
                MessageBox.Show("el campo existencias esta vacio");
                TxbExistencias.Focus();
            }
            else if (TxbFecha.Text == "")
            {
                MessageBox.Show("el campo fecha esta vacio");
                TxbFecha.Focus();
            }
            else if (TxbCantidad.Text == "")
            {
                MessageBox.Show("el campo cantidad esta vacio");
                TxbCantidad.Focus();
            }
            else if (TxbId_Proyecto.Text == "")
            {
                MessageBox.Show("el campo id_proyecto esta vacio");
                TxbId_Proyecto.Focus();
            }

            else
            {


                SqlCommand cmd = new SqlCommand("insert into material(id_material, descripcion,costos,unidad,stockminimo,existencias,fecha,cantidad,id_proyecto)Values('" + Convert.ToInt16(TxbId_Materiales) + "','" + TxbDescripcion.Text + "','" + TxbCostos.Text + "', '" + TxbUnidad.Text + "', '" + TxbStockminimo + "', '" + TxbExistencias +  "', '" + TxbFecha +  "', '" + TxbCantidad + Convert.ToInt16(TxbId_Proyecto) + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se agrego con exito ");
            }  
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

            SqlCommand cmd = new SqlCommand("update materiales set id_material ='" + TxbId_Materiales.Text + "', descripcion = '" + TxbDescripcion.Text + "', costos = '" + TxbCostos.Text + "',  Unidad = '" + TxbUnidad.Text + "',  stockminimo = '" + TxbStockminimo.Text + "',  existencias = '" + TxbExistencias.Text + "',  fecha = '" + TxbFecha.Text + "',  cantidad = '" + TxbCantidad.Text + "' where id_proyecto = '" + TxbId_Proyecto.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();    
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");
            try
            {
                SqlCommand comando = new SqlCommand("insert into material values (@id_material,@descripcion,@costos,@unidad,@stockmonimo,@existencias,@fecha,@cantidad,id_proyecto)", con);
                comando.Parameters.AddWithValue("id_materiales", TxbId_Materiales.Text);
                comando.Parameters.AddWithValue("descripcion", TxbDescripcion.Text);
                comando.Parameters.AddWithValue("costos", TxbCostos.Text);
                comando.Parameters.AddWithValue("unidad", TxbUnidad.Text);
                comando.Parameters.AddWithValue("stockminimo", TxbStockminimo.Text);
                comando.Parameters.AddWithValue("existencias", TxbExistencias.Text);
                comando.Parameters.AddWithValue("fecha", TxbFecha.Text);
                comando.Parameters.AddWithValue("cantidad", TxbCantidad.Text);
                comando.Parameters.AddWithValue("id_proyecto", TxbId_Proyecto.Text);
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos eliminados");

            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");
                DataTable dt = new DataTable();
                SqlCommand consultar = new SqlCommand("SELECT * FROM materiales", con);
                SqlDataAdapter da = new SqlDataAdapter(consultar);
                da.Fill(dt);
                consultar.Connection = con;
                dataGridView1.DataSource = dt;
                consultar.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
