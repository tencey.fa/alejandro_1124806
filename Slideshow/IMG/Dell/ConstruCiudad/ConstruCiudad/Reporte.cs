﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ConstruCiudad
{
    public partial class Reporte : Form
    {
        public Reporte()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

           // if (TxbId_reporte.Text == "")
            {
            //    MessageBox.Show("el campo Id reporte esta  vacion");
            //    TxbId_reporte.Focus();


            }
           // else if (TxbProyecto.Text == "")
            {
           //     MessageBox.Show("el campo codigo Proyecto esta vacio");
          //      TxbProyecto.Focus();
            }
          //  else if (TxbCliente.Text == "")
            {
           //     MessageBox.Show("el campo codigo Cliente esta vacio");
           //     TxbCliente.Focus();
            }
          //  else if (TxbMateriales.Text == "")
            {
           //     MessageBox.Show("el campo Materiales esta vacio");
           //     TxbMateriales.Focus();
            }
           // else if (TxbEmpleado.Text == "")
            {
           //     MessageBox.Show("el campo Empleado documento esta vacio");
           //     TxbEmpleado.Focus();
            }

           // else
            {
                

            //    SqlCommand cmd = new SqlCommand("insert into reporte(id_reporte,proyecto,cliente,materiales,empleado)Values('" + Convert.ToInt16(TxbId_reporte.Text) + "','" + TxbProyecto.Text + "','" + TxbCliente.Text + "', '" + TxbMateriales.Text + "', '" + Convert.ToInt16(TxbEmpleado.Text) + "' ", con);
            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    MessageBox.Show("Se   agrego con exito ");



                try
                {
                    SqlCommand comando = new SqlCommand("Insert into reporte values(@id_reporte,@proyecto,@cliente,@materiales,@empleado)", con);
                    comando.Parameters.AddWithValue("id_reporte ", TxbId_reporte.Text);
                    comando.Parameters.AddWithValue("proyecto ", TxbProyecto.Text);
                    comando.Parameters.AddWithValue("cliente ", TxbCliente.Text);
                    comando.Parameters.AddWithValue("materiales ", TxbMateriales.Text);
                    comando.Parameters.AddWithValue("empleado", TxbEmpleado.Text);
                    


                    comando.ExecuteNonQuery();
                    cargarGrid();
                    MessageBox.Show("sus datos han sido guardados");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


            }  
        }

        private void Reporte_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

            SqlCommand cmd = new SqlCommand("update reporte set proyecto ='" + TxbProyecto.Text + "', cliente = '" + TxbCliente.Text + "', materiales = '" + TxbMateriales.Text + "',  empleado = '" + TxbEmpleado.Text + "' where id_reporte = '" + TxbId_reporte.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();            
 
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");
            try
            {
                SqlCommand comando = new SqlCommand("DELETE reporte WHERE id_reporte=@id_reporte", con);
                comando.Parameters.Add("id_reporte", SqlDbType.NVarChar, 50).Value = TxbId_reporte.Text;
                comando.ExecuteNonQuery();
                cargarGrid();
            }
            catch (Exception)
            {
                MessageBox.Show("Elimina por medio de (id reporte)");
            }
        }

        public void cargarGrid()
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

            DataTable dt = new DataTable();
            SqlCommand consultar = new SqlCommand("SELECT * FROM estudiante", con);
            SqlDataAdapter da = new SqlDataAdapter(consultar);


            da.Fill(dt);

            consultar.Connection = con;
            dataGridView1.DataSource = dt;
            consultar.ExecuteNonQuery();
        }

        private void button4_Click(object sender, EventArgs e)
        {            
            try
            {
                SqlConnection con = new SqlConnection("Data Source=FR002S32;Initial Catalog=construcivil;Integrated Security=True;");

                DataTable dt = new DataTable();
                SqlCommand consultar = new SqlCommand("SELECT * FROM reporte", con);
                SqlDataAdapter da = new SqlDataAdapter(consultar);
                da.Fill(dt);
                consultar.Connection = con;
                dataGridView1.DataSource = dt;
                consultar.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
