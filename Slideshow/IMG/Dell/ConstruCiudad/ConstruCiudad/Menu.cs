﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ConstruCiudad
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void BtProyecto_Click(object sender, EventArgs e)
        {
            Proyecto frm = new Proyecto();
            frm.Show();
        }

        private void BtReportre_Click(object sender, EventArgs e)
        {
            Reporte frm = new Reporte();
            frm.Show();
        }

        private void BtEmpleado_Click(object sender, EventArgs e)
        {
            Empleado frm = new Empleado();
            frm.Show();
        }

        private void BtCliente_Click(object sender, EventArgs e)
        {
            cliente frm = new cliente();
            frm.Show();
        }

        private void BtMaterial_Click(object sender, EventArgs e)
        {
            materiales frm = new materiales();
            frm.Show();
        }

        private void BtCiudad_Click(object sender, EventArgs e)
        {
            ciudad frm = new ciudad();
            frm.Show();
        }

       
    }
}
